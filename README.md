# Workflow

## Fonts

* [Font Sarena](https://fontsarena.com/)

## Optimization

### FavIcon

* [Generate FavIcons](https://realfavicongenerator.net/)

### Images

* [SVG optimization](https://jakearchibald.github.io/svgomg/)


## Generator

* [Waves](https://getwaves.io/)
* [Blobs](https://www.blobmaker.app/)
* [Color Palette](https://coolors.co/46b1c9-84c0c6-9fb7b9-bcc1ba-f2e2d2)

## SVG

* [Path-Editor](https://yqnn.github.io/svg-path-editor/)

## Clip-path

* [Clippy](https://bennettfeely.com/clippy/)


### Animations
* [Easing Curves Cheat Sheet](https://easings.net/en#)

## Inspiration


### Scrolling
* [Reflect - Scrolling](https://www.refletcommunication.com/en/emotion)
* [Apple Watch](https://codepen.io/Hornebom/pen/zGKVva)

### Animations
* [Keppler - Nice Css](https://brunosimon.github.io/keppler/)
* [RedBull Racing](https://thenewmobileworkforce.imm-g-prod.com/on-race-day)

### WebGl

* [Portfolio - Game](http://bruno-simon.com/)

### Logo

* [CSS background change on scroll](https://codepen.io/giana/pen/PqVbRr)
* [Draw on scroll](https://codepen.io/chriscoyier/pen/YXgWam)


### Masks

* [Mask examples](https://tdm.pw/masks/)

## Styles

### Take a look
* [Tailblocks](https://mertjf.github.io/tailblocks/)

## Components

### Credit Card Form
* [Github](https://github.com/muhammederdem/credit-card-form)

## Sites

* [Curly Pictures](http://curlypictures.com/)
* [Not Sold](https://notsold.gratis/)

## Tools

* [Wordpress Management and Setup](https://localwp.com/)